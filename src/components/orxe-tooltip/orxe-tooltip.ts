import { html, LitElement, customElement, property } from 'lit-element';
const { Tooltip } = require('carbon-components');
import carbonElementScss from './orxe-tooltip.scss';
// import globalStyles from '../../styles/carbon-components.min.css';


@customElement('orxe-tooltip')
export default class HelloElement extends LitElement {

	tooltipInstance: any;

	/* This property is used for label of the tooltip */
	@property({ type: String }) label = '';

	/* This property is used to get content inside tooltip */
	@property({ type: String }) tooltipContent = '';

	/* This property is used to decide tooltip location*/
	@property({ type: String }) tooltipLocation: 'top' | 'bottom' | 'left' | 'right' = 'bottom';

	/* This property is used to get tooltip type */
	@property({ type: String }) tooltipType: 'interactive' | 'icon' | 'defination' = 'interactive';

	/* This is used to get alognement of icon tooltip and defination tooltip */
	@property({ type: String }) alignment: 'start' | 'end' | 'center' = 'center';

	/* This property is used to get id of the footer for interactive tooltip */
	@property({ type: String }) footerId = "";

	/* This property is used to get heading of the interactive tooltip */
	@property({ type: String }) heading = "";

	/* This property is used to get tooltip button icon image src for the icon tooltip and interative tooltip */
	@property({ type: String }) iconSrc = '';

	uniqueId = Math.random().toString(36).substring(2);

	footer: any;

	connectedCallback() {
		super.connectedCallback();

	}

	static styles = carbonElementScss;

	firstUpdated() {


		if (this.tooltipType === 'interactive') {
			if (this.footerId) {
				const temp: HTMLTemplateElement = <HTMLTemplateElement>document.getElementById(this.footerId);
				if (temp)
					this.footer = temp.content.cloneNode(true);
				let thisFooter = document.getElementById(`footerContent-${this.footerId}`);
				if (thisFooter)
					thisFooter.appendChild(this.footer);
				this.tooltipInstance = Tooltip.create(
					document.getElementById(`my-tooltip-trigger-${this.uniqueId}`)
				);
			} else {
				this.tooltipInstance = Tooltip.create(
					document.getElementById(`my-tooltip-trigger-${this.uniqueId}`)
				);
			}

		}

	}


	renderIconTooltip() {
		return html`
		<button
			class="bx--tooltip__trigger bx--tooltip--a11y bx--tooltip--${this.tooltipLocation} bx--tooltip--align-${this.alignment}">
			<span class="bx--assistive-text">${this.tooltipContent}</span>			
			<img src="${this.iconSrc}">
		</button>
		`;
	}

	renderDefinitionTooltip() {
		return html`
		<div class="bx--tooltip--definition bx--tooltip--a11y">
		<button aria-describedby="example-start" class="bx--tooltip__trigger bx--tooltip--a11y bx--tooltip__trigger--definition bx--tooltip--${this.tooltipLocation} bx--tooltip--align-${this.alignment}">
		${this.label}
		</button>
  		<div class="bx--assistive-text" id="example-start" role="tooltip">${this.tooltipContent}</div>
		</div>
		`;
	}


	renderTooltip() {

		return html`
		<div id="example-qm2516vmkxs-${this.uniqueId}-label" class="bx--tooltip__label">
			<label for="example-qm2516vmkxs-${this.uniqueId}">${this.label}</label>
			<button id="my-tooltip-trigger-${this.uniqueId}" aria-expanded="false" aria-labelledby="example-qm2516vmkxs-${this.uniqueId}-label" data-tooltip-trigger data-tooltip-target="#example-qm2516vmkxs-${this.uniqueId}" class="bx--tooltip__trigger" aria-controls="example-qm2516vmkxs-${this.uniqueId}" aria-haspopup="true">			
				<img src="${this.iconSrc}">
			</button>
		</div>
		<div id="example-qm2516vmkxs-${this.uniqueId}" tabindex="0" aria-hidden="true" data-floating-menu-direction="${this.tooltipLocation}" class="bx--tooltip" role="dialog" aria-describedby="example-qm2516vmkxs-${this.uniqueId}-body" aria-labelledby="example-qm2516vmkxs-${this.uniqueId}-heading">
			<span class="bx--tooltip__caret"></span>
			<div class="bx--tooltip__content">
				<h4 id="example-qm2516vmkxs-${this.uniqueId}-heading">${this.heading}</h4>
				<p id="example-qm2516vmkxs-${this.uniqueId}-body" >${this.tooltipContent}</p>
				<div class="bx--tooltip__footer" id="footerContent-${this.footerId}">		  	
			</div>
			<span tabindex="0"></span>
		</div>
		`;
	}

	render() {

		if (this.tooltipType === 'icon') {
			return this.renderIconTooltip();
		} else if (this.tooltipType === 'defination') {
			return this.renderDefinitionTooltip();
		} else if (this.tooltipType === 'interactive') {
			return this.renderTooltip();
		} else {
			return this.renderTooltip();
		}

	}

	createRenderRoot() {
		return this;
	}



}
