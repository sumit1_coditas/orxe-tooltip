# orxe-tooltip



<!-- Auto Generated Below -->


## Properties

| Property                  | Attribute                    | Description                                                                                                                                                            | Type                                                            | Default     |
| ------------------------- | ---------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------- | ----------- |
| `lable`                   | `label`                      | Tooltip button label                                                                                                                                                   | `String`                                                        | ``          |
| `tooltipContent`          | `tooltipContent`             | Tolltip content whis is view inside tooltip                                                                                                                            | `String`                                                        | ``          |
| `tooltipLocation`         | `tooltipLocation`            | Tooltip location where tooltip can be placed (`top`,`bottom`,`left`,`right`)                                                                                           | `String`                                                        | `bottom`    |
| `tooltipType`             | `tooltipType`                | This property is used to get tooltip type (`interactive`,`icon`,`defination`)                                                                                          | `String`                                                        |`interactive`|
| `alignment`               | `alignment`                  | This is used to get alognement of icon tooltip and defination tooltip (`start`,`end`,`center`)                                                                         | `String`                                                        | `center`    |
| `footerId`                | `footerId`                   | This property is used to get id of the footer for interactive tooltip                                                                                                  | `String`                                                        | ``          |
| `heading`                 | `heading`                    | This property is used to get heading of the interactive tooltip                                                                                                        | `String`                                                        | ``          |
| `iconSrc`                 | `iconSrc`                    | This property is used to get tooltip button icon image src for the icon tooltip and interative tooltip                                                                 | `String`                                                        | ``          |
