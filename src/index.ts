import "./components/orxe-tooltip/orxe-tooltip";

import { customElement, LitElement, html } from "lit-element";

@customElement("root-el")
export class RootElement extends LitElement {

  render() {
    return html`
        <orxe-tooltip 
        heading="Weather"
        footerId="interactiveFooter" 
        tooltipType="interactive" 
        label="label-tooltip- with footer and heading" 
        tooltipContent="Weather information" 
        tooltipLocation="bottom" 
        alignment="start"
        iconSrc="https://img.icons8.com/windows/32/000000/cloudflare--v2.png"
      >
      </orxe-tooltip> <br><br>
      <template id="interactiveFooter">
        <a href="" class="bx--link">More</a>
        <button class="bx--btn bx--btn--primary bx--btn--sm" type="button">See detail</button>
      </template> 
      <orxe-tooltip 
        tooltipType="icon" 
        tooltipContent="Clear Weather" 
        tooltipLocation="bottom" 
        alignment="start"
        iconSrc="https://img.icons8.com/windows/32/000000/cloudflare--v2.png"
      >
      </orxe-tooltip> <br><br>
      
      <orxe-tooltip 
        tooltipType="defination" 
        label="label-tooltip" 
        tooltipContent="Defination of this content" 
        tooltipLocation="top" 
        alignment="start" 
      >
      </orxe-tooltip> <br><br>


      <button>There you go</button><orxe-tooltip iconSrc="https://cdnjs.cloudflare.com/ajax/libs/simple-icons/3.0.1/airbnb.svg" heading="Tooltip Heading" tooltipType="interactive" label="label-tooltip" tooltipContent="content Interactive" tooltipLocation="right" alignment="start">
      </orxe-tooltip> <br><br>

    <orxe-tooltip tooltipType="icon" label="label-tooltip" iconSrc="https://img.icons8.com/windows/32/000000/cloudflare--v2.png" tooltipContent="weather" tooltipLocation="bottom" alignment="start" ></orxe-tooltip> <br><br>
    
    <orxe-tooltip tooltipType="defination" label="label-tooltip" tooltipContent="Defination of this content" tooltipLocation="bottom" alignment="start" ></orxe-tooltip> <br><br>



    `;
  }
  createRenderRoot() {
    return this;
  }
}
